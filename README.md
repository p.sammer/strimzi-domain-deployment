# Strimzi domain deployment

## Update versions on Nexus

* find relevant images. Hints:
  * running deployments/pods/containers on dev
  * `value(-*).yaml`  files
  * images [here](https://dev.azure.com/wacmes/DBP%20Si-Napse/_git/buildsystem-proxydockerimages-engineer)
* update image versions [here](https://dev.azure.com/wacmes/DBP%20Si-Napse/_git/buildsystem-proxydockerimages-engineer)
* push to master and wait for successful build
* check for changes necessary in `value(-*).yaml` files

## Dependencies

This chart pulls in dependencies. The version
used are specified in `Chart.yaml` in the `dependencies` section.
If you change the versions in there, you need to then run

    $ helm dependency update

in order to have the chart downloaded to the `charts` directory
and then also commit that new version alongside with the altered
`Chart.yaml` file.

See the [Helm docs](https://helm.sh/docs/topics/charts/#chart-dependencies)
for details.
